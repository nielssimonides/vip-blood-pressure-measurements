# VIP Blood Pressure Measurements skeleton app

This projects contains the VIP blood pressure measurements skeleton app. This project provides some plumbing to help you kickstart the 'Project Persisent' assignment. Feel free to alter this project in any way.

## Clone

Clone this project in your own Bitbucket repository so you and your team can work simultaneously on the same codebase.

## Stack

This project is built in Kotlin and uses components of the Android Jetpack stack. Although we recommend to keep making use of this stack feel free to make use of the stack you see fit.


