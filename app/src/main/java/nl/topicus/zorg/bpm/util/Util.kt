package nl.topicus.zorg.bpm.util

import android.support.annotation.IdRes
import android.view.View
import androidx.navigation.Navigation

/**
 * Helper function which makes our navigation code cleaner
 */
fun View.navigateTo(@IdRes resId: Int) = Navigation.findNavController(this).navigate(resId)

