package nl.topicus.zorg.bpm.ui.personal.care.ui.bloodpressuremeasurement

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import nl.topicus.zorg.bpm.database.BPMeasurementRepository

class BPMeasurementViewModel(private val repository: BPMeasurementRepository) :
    ViewModel()
{
    val measurements = repository.getAllMeasurements()
}

class BPMeasurementViewModelFactory(private val repository: BPMeasurementRepository)
    : ViewModelProvider.NewInstanceFactory()
{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = BPMeasurementViewModel(
        repository) as T
}