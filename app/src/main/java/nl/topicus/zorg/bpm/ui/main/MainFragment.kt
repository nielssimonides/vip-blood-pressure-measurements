package nl.topicus.zorg.bpm.ui.main

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.main_fragment.bloodpressuremeasurement_button
import nl.topicus.zorg.bpm.R
import nl.topicus.zorg.bpm.util.navigateTo

class MainFragment : Fragment()
{
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View
    {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        bloodpressuremeasurement_button.setOnClickListener { view ->
            view.navigateTo(R.id.action_mainFragment_to_bloodPressureMeasurementFragment)
        }
    }
}
