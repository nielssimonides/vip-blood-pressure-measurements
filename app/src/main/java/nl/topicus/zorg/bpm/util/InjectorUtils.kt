package nl.topicus.zorg.bpm.util

import android.content.Context
import nl.topicus.zorg.bpm.database.AppDatabase
import nl.topicus.zorg.bpm.database.BPMeasurementRepository
import nl.topicus.zorg.bpm.ui.personal.care.ui.bloodpressuremeasurement.BPMeasurementViewModelFactory

/**
 * This object contains helper functions which contructs ViewModelFactories which we will need to access the ViewModels inside the UI
 */
object InjectorUtils
{
    fun provideBPMeasurementModelFactory(context: Context): BPMeasurementViewModelFactory
    {
        val repository =
            BPMeasurementRepository.getInstance(AppDatabase.getInstance(context).measurementDao())
        return BPMeasurementViewModelFactory(repository)
    }
}