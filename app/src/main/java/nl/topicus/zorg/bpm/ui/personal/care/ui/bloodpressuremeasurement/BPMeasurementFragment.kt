package nl.topicus.zorg.bpm.ui.personal.care.ui.bloodpressuremeasurement

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nl.topicus.zorg.bpm.R
import nl.topicus.zorg.bpm.util.InjectorUtils

class BPMeasurementFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View
    {

        val view = inflater.inflate(R.layout.blood_pressure_measurement_fragment, container, false)

        val factory = InjectorUtils.provideBPMeasurementModelFactory(requireActivity())
        val viewModel = ViewModelProviders.of(this, factory)
            .get(BPMeasurementViewModel::class.java)

        viewModel.measurements.observe(this, Observer {

            // TODO Update the (yet to be constructed) UI with the new measurements
        })

        return view
    }
}
