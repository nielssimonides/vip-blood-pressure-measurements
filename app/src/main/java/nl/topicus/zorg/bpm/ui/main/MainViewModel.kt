package nl.topicus.zorg.bpm.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * This ViewModel is quite empty because there is only one menu item in this skeleton app
 */
class MainViewModel : ViewModel()
{
    private val _data = MutableLiveData<String>()
    val data: LiveData<String>
        get() = _data

    init
    {
        _data.value = "Hello, VIP"
    }
}
