package nl.topicus.zorg.bpm.database

import android.arch.persistence.room.TypeConverter
import java.util.Calendar

class Converters
{
    @TypeConverter
    fun calendarToDatestamp(calendar: Calendar): Long = calendar.timeInMillis

    @TypeConverter
    fun datestampToCalendar(value: Long): Calendar =
        Calendar.getInstance().apply { timeInMillis = value }
}