package nl.topicus.zorg.bpm.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.Query
import java.util.Calendar

/**
 * The model of het persisted measurments
 */
@Entity
class BPMeasurement(
    @PrimaryKey val id: Int,
    val date: Calendar
    // TODO Fill with other properties needed for a measurement
)

/**
 * The BPMeasurementDao holds all the measurment queries
 */
@Dao
interface BPMeasurementDao
{
    @Query("SELECT * FROM bpmeasurement ORDER BY date")
    fun getAllMeasurements(): LiveData<List<BPMeasurement>>
}

/**
 * The repository abstracts access to the real datasource (in this case the BPMeasurementDao).
 */
class BPMeasurementRepository(private val measurementDao: BPMeasurementDao)
{
    fun getAllMeasurements() = measurementDao.getAllMeasurements()

    companion object
    {
        @Volatile
        private var instance: BPMeasurementRepository? = null

        fun getInstance(measurementDao: BPMeasurementDao) =
            instance ?: synchronized(this) {
                instance ?: BPMeasurementRepository(measurementDao).also { instance = it }
            }
    }
}